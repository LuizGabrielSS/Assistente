import requests as rq
import webbrowser as web 
from meraki_sdk.meraki_sdk_client import MerakiSdkClient
import requests
import random 
import cv2
import json 
import smtplib
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
import time
from datetime import datetime

version = "2.4.0"
cidade = 'RIO DE JANEIRO'
caminho_navegador = "C:/Users/Public/Desktop/Google Chrome %s"

def intro():
	msg = "Assistente - version {} / by: Luiz Gabriel".format(version)
	print("-" * len(msg) +  "\n{}\n".format(msg)  +   "-" * len(msg))
lista_erros = [
		"Não entendi nada",
		"Desculpe, não entendi",
		"Repita novamente por favor"
		"Desculpa, poderia repitir por favor?"
]
conversas = {
	"Olá": "oi, tudo bem?",
	"sim e você": "Estou bem obrigada por perguntar",
}
comandos = {
	"desligar": "desligando",
	"reiniciar": "reiniciando"
}
#FUNÇÕES QUE VERIFICAM SE O USUARIO JA EXISTE PARA A ASSISTENTE 
def verifica_nome(user_name):
	if user_name.startswith("Meu nome é"):
		user_name = user_name.replace("Meu nome é", "")
	if user_name.startswith("Eu me chamo"):
		user_name = user_name.replace("Eu me chamo", "")
	if user_name.startswith("Eu sou o"):
		user_name = user_name.replace("Eu sou o", "")
	if user_name.startswith("Eu sou a"):
		user_name = user_name.replace("Eu sou a", "")

	return user_name 
def verifica_nome_exist(nome):
	dados = open("dados/nomes.txt", "r")
	nome_list = dados.readlines()

	if not nome_list:
		vazio = open("dados/nomes.txt", "r")
		conteudo = vazio.readlines()
		conteudo.append("{}".format(nome))
		vazio = open("dados/nomes.txt", "w")
		vazio.writelines(conteudo)
		vazio.close()

		return "Olá {}, prazer em te conhecer!".format(nome)

	for linha in nome_list:
		if linha == nome:
			return "Olá {}, acho que já nos conhecemos".format(nome)

	vazio = open("dados/nomes.txt", "r")
	conteudo = vazio.readlines()
	conteudo.append("\n{}".format(nome))
	vazio = open("dados/nomes.txt", "w")
	vazio.writelines(conteudo)
	vazio.close()

	return "Oi {} é a primeira vez que nos falamos".format(nome)
def name_list():
	try:
		nomes = open("dados/nomes.txt", "r")
		nomes.close()

	except FileNotFoundError:
		nomes = open("dados/nomes.txt", "w")
		nomes.close()
#CALCULADORA
def calcula(entrada):
	if "mais" in entrada or "+" in entrada:
		# É soma
		entradas_recebidas = entrada.split(" ")
		resultado = int(entradas_recebidas[1]) + int(entradas_recebidas[3])

	elif "menos" in entrada or "-" in entrada:
		# É subtração

		entradas_recebidas = entrada.split(" ")
		resultado = int(entradas_recebidas[1]) - int(entradas_recebidas[3])

	elif "vezes" in entrada or "x" in entrada:
		# É vezes

		entradas_recebidas = entrada.split(" ")
		resultado = round(float(entradas_recebidas[1]) * float(entradas_recebidas[3]), 2)

	elif "dividido" in entrada or "/" in entrada:
		# É divisão

		entradas_recebidas = entrada.split(" ")
		resultado = round(float(entradas_recebidas[1]) / float(entradas_recebidas[4]), 2)

	else:

		resultado = "Operação não encontrada"


	return resultado
#CLIMA
def clima_tempo():	
	endereco_api = "http://api.openweathermap.org/data/2.5/weather?appid=9e1280f88eef9db700e867bb898fd3ec&q="
	url = endereco_api + cidade

	infos = rq.get(url).json()

	# Coord
	longitude = infos['coord']['lon']
	latitude = infos['coord']['lat']
	# main
	temp = infos['main']['temp'] - 273.15 # Kelvin para Celsius
	pressao_atm = infos['main']['pressure'] / 1013.25 #Libras para ATM
	humidade = infos['main']['humidity'] # Recebe em porcentagem
	temp_max= infos['main']['temp_max'] - 273.15 # Kelvin para Celsius
	temp_min = infos['main']['temp_min'] - 273.15 # Kelvin para Celsius

	#vento
	v_speed = infos['wind']['speed'] # km/ h
	v_direc = infos['wind']['deg'] #Recebe em graus

	#clouds / nuvens

	nebulosidade = infos['clouds']['all']

	#id
	id_da_cidade = infos['id']

	# 11
	return [longitude, latitude, 
		temp, pressao_atm, humidade, 
		temp_max, temp_min, v_speed, 
		v_direc, nebulosidade, id_da_cidade]
#TEMPO
def temperatura():
	temp_atual = clima_tempo()[2]
	temp_max = clima_tempo()[5]
	temp_min = clima_tempo()[6]
	
	return [temp_atual, temp_max, temp_min]
#ABRI NAVEGADOR
def abrir(fala):
	try:
		if "google" in fala:
			web.get(caminho_navegador).open("https://www.google.com/")
			return "abrindo google"
		elif "facebook" in fala:
			web.get(caminho_navegador).open("hhtps://facebook.com.br/")
			return "abrindo facebook"
		else:
			return "site não cadastrado para aberturas"
	except:
		return "houve um erro"
#EMAIL
def email(entrada):
	#Pega o endereço de email de destino 
	email = entrada
	emails = []
	texto = email.split(" ")
	for i in texto:
		#print(i)
		emails.append(i)
	#print(cameras))
	numero = emails.index('para')
	#print(cameras[mero+1])
	email = emails[numero+1]

	#defini o template
	numeros = emails.index("Template")
	Template = emails[numeros+1]

	from email_template import Template
	email_from = 'luizgabriel1022001@gmail.com'
	email_to = email
	email_password = 'zgxpqcnlctpaqech'
	
	smtp = 'smtp.gmail.com'
	server = smtplib.SMTP(smtp, 587)
	server.starttls()
	server.login(email_from, email_password)

	message = MIMEMultipart()
	message["From"] = "Segurança Física | Stone"
	message["To"] = email_to
	message["Subject"] = "[Segurança] Detector encontrou alguem sem mascara"

	Template = Template.replace('NOME_EMPRESA',str(email_to))
	#CRIAR VARIAVEIS PARA QUE CADA EMAIL POSSA SER ENVIADO DE FORMA ESPECIFICA
	Template = Template.replace('Bom dia, Boa tarde ou Boa Noite', hora())
	message.attach(MIMEText(Template, 'html'))

	server.sendmail(email_from, email_to, message.as_string())
	server.quit()
	print('email enviado com sucesso')
#horario
def hora():
    now = datetime.now()
    x = now.hour
    if 6 <= x < 12:
        return "Bom dia"
    elif 12 <= x <= 19:
        return "Boa tarde"
    elif 20 <= x <= 00:
        return "Boa noite"
    else:
        return "Boa madrugada"		